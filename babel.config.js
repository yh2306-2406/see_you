/*
 * @Author: fleeting-ln 3518413017@qq.com
 * @Date: 2023-11-14 22:04:15
 * @LastEditors: fleeting-ln 3518413017@qq.com
 * @LastEditTime: 2023-11-15 19:53:21
 * @FilePath: \see_you\babel.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
  ['import', {
    libraryName: 'vant',
    libraryDirectory: 'es',
    style: true
  }, 'vant']
}
