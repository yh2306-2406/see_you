/***
 * @Author: XY
 * @Date: 2023-11-09 16:20:14
 * @LastEditors: XY
 * @LastEditTime: 2023-12-08 14:59:16
 * @FilePath: \friendsApp\see_you\src\api\index.js
 **/
import request from "@/utils/request";
// 如果需要分模块api,自行拆分

//注册设备
export function device(paraws) {
  return request({
    url: '/api/device',
    method: 'post',
    data: paraws
  })
}

//探测用户是否开通IM权限
export function enterim() {
  return request({
    url: '/api/enter/im',
    method: 'get',
  })
}

//登录
export function login(paraws) {
  return request({
    url: '/api/login/mobile',
    method: 'post',
    data: paraws
  })
}


//发送验证码
export function verify(paraws) {
  return request({
    url: '/api/register/verify',
    method: 'post',
    data: paraws
  })
}

//获取城市数据
export function cityList(paraws) {
  return request({
    url: '/api/city_list',
    method: 'get',
    data: paraws
  })
}

//获取热门城市数据
export function hotCity(paraws) {
  return request({
    url: '/api/hot_city',
    method: 'get',
    data: paraws
  })
}

//上传定位
export function positioning(paraws) {
  return request({
    url: '/api/positioning',
    method: 'post',
    data: paraws
  })
}

// 首页数据
export function getReigonTree(params) {
  return request({
    url: `/api/home?${params}`,
    method: "get",
    hideloading: true,
  });
}
// 广场数据
export function getPiazza(params) {
  return request({
    url: `/api/piazza?${params}`,
    method: "get",
    hideloading: true,
  });
}
//查看用户主页信息
export function getUserInfo(params) {
  return request({
    url: `/api/user/${params.id}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    method: "get",
    hideloading: true,
  });
}

//查看用户信息
export function getMyInfo() {
  return request({
    url: `/api/userinfo`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    method: "get",
    hideloading: true,
  });
}
//用户相册
export function getAlbumList() {
  return request({
    url: `/api/user/album`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    method: "get",
    hideloading: true,
  });
}
//删除用户相册
export function delAlbumList(params) {
  return request({
    url: `/api/user/album/del`,

    data: {
      album: params
    },
    method: "post",
    hideloading: true,
  });
}
//添加用户相册
export function addAlbumList(params) {
  return request({
    url: `/api/user/album`,

    method: "post",
    data: params,
  });
}

//获取访客数据
export function getGlanceList(params) {
  return request({
    url: `/api/user/visit_list?${params}`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    method: "get",
    hideloading: true,
  });
}

// 添加商家服务点评
export function addUserReply(params) {
  return request({
    url: "/api/user/reply",
    method: "post",
    data: params,
    hideloading: true,
  });
}

// 商家服务点评列表
export function getReplyList(params) {
  return request({
    url: "/api/reply/list",
    method: "get",
    params: params,
    hideloading: true,
  });
}
//查看商家微信或手机号（扣除记录）
export function getMerChant(merchant_id) {
  return request({
    url: `/api/enter/secrecy/see_save/${merchant_id}`,
    method: "get",
    hideloading: true,
  });
}
//探测用户查看商家手机号权限
export function getEnterSecrecyPhone(merchant_id) {
  return request({
    url: `/api/enter/secrecy/phone/${merchant_id}`,
    method: "get",
    hideloading: true,
  });
}

//我的发布&他人发布的动态
export function getDynamicList(id, params) {
  return request({
    url: `/api/dynamic/list/${id}`,
    method: "get",
    params: params,
    hideloading: true,
  });
}

//用户首次信息完善
export function complete(params) {
  return request({
    url: `/api/user/complete`,
    method: "post",
    data: params,
  });
}
//商家点评用户列表
export function getEnterReply(params) {
  return request({
    url: `api/enter/reply/list/${params.uid}`,
    method: "get",
    params: params,
    hideloading: true,
  });
}

// 添加商家点评用户印象  
export function addEnterReply(params) {
  return request({
    url: `/api/enter/reply`,
    method: "post",
    data: params,
    hideloading: true,
  });
}

// 添加商家点评用户印象  
export function userFollow(params) {
  return request({
    url: `/api/user/follow`,
    method: "post",
    data: params,
    hideloading: true,
  });
}

//获取协议数据
export function getxieyi(params) {
  return request({
    url: `/api/pc/get_agreement?${params}`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}
//获取黑名单
export function getback(params) {
  return request({
    url: `/api/black/list`,
    method: "get",
    // data: ,
    params: params,
    hideloading: true,
  });
}
export function delback(params) {
  return request({
    url: `/api/black/${params}`,
    method: "get",
    // data: ,
    // hideloading: true,
  });
}
//获取钻石收入明细
export function getCommission(params) {
  return request({
    url: `/api/spread/commission`,
    method: "get",
    // data: ,
    data: params,
    hideloading: true,
  });
}


//获取邀请好友列表
export function getSpreadList(params) {
  return request({
    url: `/api/spread/list`,
    method: "get",
    // data: ,
    data: params,
    hideloading: true,
  });
}
//获取邀请页面
export function getSpread(params) {
  return request({
    url: `/api/user/invitation`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}
//获取动态列表
export function getDongtai(params) {
  return request({
    url: `/api/dynamic/list/${params}`,
    method: "get",
    data: params,
    hideloading: true,
  });
}
export function getViplist() {
  return request({
    url: `/api/user/member/record`,
    method: 'get',

  })
}
export function getUserList(params) {
  return request({
    url: `/api/user/friends`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    },
    params: params,
    method: "get",
    hideloading: true,
  });
}

//用户信息修改
export function useredit(params) {
  return request({
    url: `/api/user/edit`,
    method: 'post',
    data: params,
  })
}

//删除动态列表
export function delDongtai(params) {
  return request({
    url: `/api/user/dynamic/${params}`,
    method: "get",
    hideloading: true,
  });
}
//申请商家
export function applybusiness(params) {
  return request({
    url: `/api/enter/apply`,
    method: "post",
    data: params,
    hideloading: true,
  });
}

//我的点评列表
export function getimpressList(params) {
  return request({
    url: `/api/enter/impress/list`,
    method: "get",
    data: params,
    hideloading: true,
  });
}
//删除我的点评列表

export function delimpressList(params) {
  return request({
    url: `/api/enter/impress/${params}`,
    method: "get",
    hideloading: true,
  });
}

//获取商家信息
export function getimpressInfo() {
  return request({
    url: `/api/enter/apply`,
    method: "get",
    hideloading: true,
  });
}

//用户与商家聊天（扣除记录）
export function getChatSave(merchant_id, params) {
  if (params) {
    return request({
      url: `/api/enter/secrecy/chat_save/${merchant_id}`,
      method: "get",
      data: params,
      hideloading: true,
    });
  }else{
    return request({
      url: `/api/enter/secrecy/chat_save/${merchant_id}`,
      method: "get",
      hideloading: true,
    });
  }
}




