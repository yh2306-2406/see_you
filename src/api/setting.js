/***
 * @Author: XY
 * @Date: 2023-11-15 17:18:45
 * @LastEditors: XY
 * @LastEditTime: 2023-11-22 14:41:54
 * @FilePath: \friendsApp\see_you\src\api\setting.js
 **/
import request from "@/utils/request";
// 如果需要分模块api,自行拆分

//上传定位
export function setPositioning(params) {
  return request({
    url: '/api/positioning',
    method: 'post',
    params: params
  })
}

// 图片上传
export function uploadImg(params) {
  return request({
    url: "/api/upload/image",
    method: "post",
    data: params,
    headers: {
      'Content-Type': 'application/form-data'
    },
    hideloading: true,
  });
}

//添加黑名单
export function setBlack(paraws) {
  return request({
    accepttype: '', //请求头body的类型
    url: '/api/black',
    method: 'post',
    params: paraws
  })
}

//用户举报
export function setServiceReport(paraws) {
  return request({
    url: '/api/user/service/report',
    method: 'post',
    data: paraws
  })
}

//用户反馈
export function setServiceFeedback(paraws) {
  return request({
    url: '/api/user/service/feedback',
    method: 'post',
    data: paraws
    
  })
}


//用户举报
export function getSiteConfig() {
  return request({
    url: '/api/site_config',
    method: 'get',
  })
}
//用户设置
export function getSetting() {
  return request({
    url: `/api/user/setting/get`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}
//修改用户设置
export function SetSetting(paraws) {
  return request({
    url: `/api/user/setting/set`,
    method: "post",
    // data: ,
    data: paraws,
    hideloading: true,
  });
}
//探测用户是否开通IM权限
export function getim() {
  return request({
    url: `/api/enter/im`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}
//注销用户信息
export function deluser() {
  return request({
    url: `/api/user_cancel`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}

//用户与商家聊天（扣除记录）
export function ReduceChat(paraws) {
  return request({
    url: `/api/enter/secrecy/chat_save/${paraws.uid}`,
    method: "get",
    data: {
      user_type:paraws.type
    },
    hideloading: true,
  });
}

//探测用户是否有聊天权限
export function chatOk(paraws) {
  return request({
    url: `/api/user/secrecy/chat/${paraws}`,
    method: "get",
    // data: ,
    hideloading: true,
  });
}


// 发布动态/修改动态
export function sendUserDynamic(paraws) {
  return request({
    url: `/api/user/dynamic${paraws.id ? '/' + paraws.id : ''}`,
    method: 'post',
    data: paraws
  })
}

// 会员权益
export function vipinfo(paraws) {
  return request({
    url: `/api/pc/benefits`,
    method: 'get',
    params:{
      token:paraws
    }
  })
}

// 身份证回传
export function Authentication(paraws) {
  return request({
    url: `/api/authentication`,
    method: 'post',
    params:paraws
  })
}
 

// 修改手机号
export function editPhone(paraws) {
  return request({
    url: `/api/user/updatePhone`,
    method: 'post',
    data:paraws
  })
}




// 获取百度临时token
export function GetVerifyToken(paraws) {
  return request({
    url: `/api/baidu/verify_token`,
    method: 'GET',
   
  })
}
// 获取百度验证结果
export function Getrenzhen(paraws) {
  return request({
    url: `/api/baidu/notify?token=${paraws}`,
    method: 'GET',
   
  })
}
