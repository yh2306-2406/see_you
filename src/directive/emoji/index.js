/**
 *
 * 开发中遇到的表单输入，往往会有对输入内容的限制，比如不能输入表情和特殊字符，只能输入数字或字母等。
 * 我们常规方法是在每一个表单的 on-change 事件上做处理。这样代码量比较大而且不好维护
 * 需求：
 *     根据正则表达式，设计自定义处理表单输入规则的指令，下面以禁止输入表情和特殊字符为例。
 */

let findEle = (parent, type) => {
    return parent.tagName.toLowerCase() === type
        ? parent
        : parent.querySelector(type);
};

const trigger = (el, type) => {
    const e = document.createEvent("htmlEvents");
    e.initEvent(type, true, true);
    el.dispatchEvent(e);
};

export default {
    bind: function (el, binding, vnode) {
        // 正则规则可根据需求自定义 
        // var regRule = /[^u4E00-u9FA5|d|a-zA-Z|rns,.?!，。？！…—&$=()-+/*{}[]]|s/g;
        var regRule = /[^\u4E00-\u9FA5\dA-Za-z]|/g; // 只能输入中文,英文和数字
        let $inp = findEle(el, "input");
        el.$inp = $inp;
        $inp.handle = function () {
            let val = $inp.value;
            $inp.value = val.replace(regRule, "");
            trigger($inp, "input");
        };
        $inp.addEventListener("keyup", $inp.handle);
    },
    unbind: function (el) {
        el.$inp.removeEventListener("keyup", el.$inp.handle);
    },
};
