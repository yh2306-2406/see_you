/**
 * 用来判断一个DOM元素是否有内容,有内容则显示内容,没有内容则显示指令内容
 * 如果传过来的对象或者数组为空,则不显示该DOM元素
 */
// 判断对象或数组是否为空
function isEmpty(obj) {
    if (Array.isArray(obj)) {
        return obj.length === 0;
    } else if (typeof obj === "object") {
        return Object.keys(obj).length === 0;
    }
    return true; // 其他类型默认为空
}
export default {
    inserted: function (el, binding) {
        const value = binding.value;
        if (isEmpty(value)) {
            el.style.display = "none";
        }
    },
};
