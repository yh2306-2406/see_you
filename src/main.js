import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Vant from 'vant';

import 'vant/lib/index.css';
import "./utils/flexible" // 移动端适配文件
import directive from './directive' // 自定义全局指令
import "./style/base.css" // 引入基础样式
import "./style/common.css" // 引入公共样式
// import "@/utils/WebIM"
import WebIM from '@/utils/WebIM';

// import EC from 'easemob-websdk'

// const conn = new EC.connection({
// 	appKey: '1120230610161163#seyouplay',
// });


// Vue.prototype.$conn = conn;
Vue.use(Vant);
Vue.use(directive)
Vue.config.productionTip = false

new Vue({
  WebIM,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
