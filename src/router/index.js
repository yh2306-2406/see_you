/***
 * @Author: XY
 * @Date: 2023-11-09 16:20:14
 * @LastEditors: XY
 * @LastEditTime: 2023-11-22 11:03:27
 * @FilePath: \friendsApp\see_you\src\router\index.js
 **/
import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/user/Login'
import Cookies from "js-cookie";
import store from '@/store/index'

Vue.use(VueRouter)

const routes = [
  { path: '*', redirect: '/login' },
  {
    path: '/',
    redirect: "/Login",
  },
  {
    path: '/Login',
    name: 'Login',
    component: Login,
    meta: {
      title: '',
      icon: '',
      isshow: false, //是否显示页面上方导航
      istabbar: false, //是否显示底部导航
    }
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('@/views/homePage/index.vue'),
    meta: {
      title: '首页',
      icon: '',
      keepAlive: true,
      isshow: false,
      istabbar: true,
    }
  },

  {
    path: '/piazza',
    name: 'piazza',
    component: () => import('@/views/homePage/piazza.vue'),
    meta: {
      title: '广场',
      icon: '',
      keepAlive: true,
      isshow: false,
      istabbar: true,
    }
  },
  {
    path: '/vip/buylist',
    name: 'buylist',
    component: () => import('@/views/my/vip/buylist.vue'),
    meta: {
      title: '购买记录',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/setting/xieyi',
    name: 'xieyi',
    component: () => import('@/views/my/setting/xieyi.vue'),
    meta: {
      title: '协议',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/xieyi',
    name: 'xieyi1',
    component: () => import('@/views/my/xieyi.vue'),
    meta: {
      title: '平台协议',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/invitation',
    name: 'invitation',
    component: () => import('@/views/my/invitation.vue'),
    meta: {
      title: '邀请好友',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/setting/blacklist',
    name: 'blacklist',
    component: () => import('@/views/my/setting/blacklist.vue'),
    meta: {
      title: '黑名单',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/setting/privacy',
    name: 'privacy',
    component: () => import('@/views/my/setting/privacy.vue'),
    meta: {
      title: '隐私设置',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/setting/notice',
    name: 'notice',
    component: () => import('@/views/my/setting/notice.vue'),
    meta: {
      title: '通知设置',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/glance',
    name: 'setting',
    component: () => import('@/views/my/glance.vue'),
    meta: {
      title: '访客足迹',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/income',
    name: 'income',
    component: () => import('@/views/my/income.vue'),
    meta: {
      title: '钻石收入明细',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/my/detail',
    name: 'Mydetail',
    component: () => import('@/views/my/detail.vue'),
    meta: {
      title: '我的商家',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/attestation',
    name: 'attestation',
    component: () => import('@/views/my/attestation/index.vue'),
    meta: {
      title: '实名认证',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/mytrends',
    name: 'success',
    component: () => import('@/views/my/mytrends.vue'),
    meta: {
      title: '我的动态',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/attestation/success',
    name: 'success',
    component: () => import('@/views/my/attestation/success.vue'),
    meta: {
      title: '实名认证',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/attestation/error',
    name: 'error',
    component: () => import('@/views/my/attestation/error.vue'),
    meta: {
      title: '实名认证',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/chat/list',
    name: 'chatlist',
    component: () => import('@/views/chat/list.vue'),
    meta: {
      title: '信息',
      icon: '',
      isshow: false,
      istabbar: true,
    }
  },{
    path: '/chat/index',
    name: 'systemInfo',
    component: () => import('@/views/chat/index.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/business',
    name: 'business',
    component: () => import('@/views/my/business.vue'),
    meta: {
      title: '商家信息',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/chat/systemInfo',
    name: 'systemInfo',
    component: () => import('@/views/chat/systemInfo.vue'),
    meta: {
      title: '系统信息',
      icon: '',
      isshow: true,
      istabbar: true,
    }
  },
  {
    path: '/mydianping',
    name: 'mydianping',
    component: () => import('@/views/my/mydianping.vue'),
    meta: {
      title: '我的点评',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/invite',
    name: 'invite',
    component: () => import('@/views/my/invite.vue'),
    meta: {
      title: '有效邀请好友',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/setting/user',
    name: 'user',
    component: () => import('@/views/my/setting/user.vue'),
    meta: {
      title: '账号管理',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },{
    path: '/album',
    name: 'user',
    component: () => import('@/views/my/album.vue'),
    meta: {
      title: 'ta的相册',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/vip/Info',
    name: 'vipInfo',
    component: () => import('@/views/my/vip/vipInfo.vue'),
    meta: {
      title: '会员中心',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/setting/feedback',
    name: 'feedback',
    component: () => import('@/views/my/setting/feedback.vue'),
    meta: {
      title: '反馈建议',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/my',
    name: 'my',
    component: () => import('@/views/my/my.vue'),
    meta: {
      title: '我的',
      icon: '',
      isshow: false,
      istabbar: true,
    }
  }
  , {
    path: '/setting',
    name: 'setting',
    component: () => import('@/views/my/setting.vue'),
    meta: {
      title: '设置',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('@/views/homePage/search.vue'),
    meta: {
      title: '搜索',
      icon: '',
      keepAlive: true,
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/detail',
    name: 'detail',
    component: () => import('@/views/homePage/detail.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: true,
      istabbar: false,
      isRight: true
    }
  },
  {
    path: '/userDetail',
    name: 'userDetail',
    component: () => import('@/views/homePage/userDetail.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: true,
      istabbar: false,
      isRight: true

    }
  },
  {
    path: '/settingPage/inform',
    name: 'inform',
    component: () => import('@/views/settingPage/inform.vue'),
    meta: {
      title: '举报',
      icon: '',
      isshow: true,
      istabbar: false,
    }
  },
  {
    path: '/settingPage/setting',
    name: 'setting',
    component: () => import('@/views/settingPage/setting.vue'),
    meta: {
      title: '设置',
      icon: '',
      isshow: true,
      istabbar: false,
    }
  },
 
  {
    path: '/userlist',
    name: 'userlist',
    component: () => import('../views/my/userlist.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/PerfectInformation',
    name: 'PerfectInformation',
    component: () => import('../views/user/PerfectInformation.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/PersonalData',
    name: 'PersonalData',
    component: () => import('../views/user/PersonalData.vue'),
    meta: {
      title: '',
      icon: '',
      keepAlive: true,
      isshow: false,
      istabbar: false,
    }
  },
  {
    path: '/location',
    name: 'location',
    component: () => import('../views/user/location.vue'),
    meta: {
      title: '',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  }, {
    path: '/dynamic',
    name: 'dynamic',
    component: () => import('../views/my/dynamic.vue'),
    meta: {
      title: '发布动态',
      icon: '',
      isshow: true,
      istabbar: false,
    }
  },
  {
    path: '/editProfile',
    name: 'editProfile',
    component: () => import('../views/user/editProfile.vue'),
    meta: {
      title: '编辑资料',
      icon: '',
      isshow: false,
      istabbar: false,
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  let toPath = to.path;
  console.log(to.path)
  if (to.path == '/Login') {
    next()
  } else if (store.state.token) {
    if (store.state.userInfo) {
      console.log(store.state.userInfo  )
      if (to.path == '/PerfectInformation' && store.state.userInfo.is_complete == 0 ) {
        next()
      } else {
        next()
      }
    } else {
      store.commit('SET_TOKEN', '');
    }
  } else {
    next("/Login")
  }
})

// import VueRouter from 'vue-router'
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}


export default router