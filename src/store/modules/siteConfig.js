/***
 * @Author: XY
 * @Date: 2023-11-14 16:52:41
 * @LastEditors: XY
 * @LastEditTime: 2023-11-16 10:38:13
 * @FilePath: \friendsApp\see_you\src\store\modules\siteConfig.js
 **/
import { getSiteConfig } from "@/api/setting";

const state = {
  //职业标签
  occupation_data: [{
    "id": 1045,
    "occupation_name": "美容"
  },
  {
    "id": 1044,
    "occupation_name": "自由职业"
  },
  {
    "id": 1043,
    "occupation_name": "制造业"
  },
  {
    "id": 1042,
    "occupation_name": "IT"
  },
  {
    "id": 1041,
    "occupation_name": "无业游民"
  },
  {
    "id": 1040,
    "occupation_name": "天选之子"
  },
  {
    "id": 1039,
    "occupation_name": "高手"
  }],
  //兴趣标签
  interest_label: [{
    "id": 12,
    "label_name": "看看",
    "label_icon": "https://static.seyou1.vip/attach/2023/06/48629202306172230026836.png",
    "label_no_icon": ""
  },
  {
    "id": 13,
    "label_name": "兴趣1",
    "label_icon": "",
    "label_no_icon": ""
  },
  {
    "id": 14,
    "label_name": "兴趣2",
    "label_icon": "",
    "label_no_icon": ""
  },
  {
    "id": 15,
    "label_name": "兴趣3",
    "label_icon": "",
    "label_no_icon": ""
  }],
  //服务标签
  service_label: [{
    "id": 10,
    "label_name": "伴游"
  },
  {
    "id": 11,
    "label_name": "看电影"
  }],
  //印象标签
  picture_label: [{
    "id": 8,
    "label_name": "身材好"
  },
  {
    "id": 9,
    "label_name": "素质高"
  }],
  //评论标签
  assess_label: [{
    "id": 16,
    "label_name": "评价1"
  },
  {
    "id": 17,
    "label_name": "评价2"
  }],
  //采集信息说明
  information_collection: "",
  //第三方说明
  third_description: "",
  //用户隐私
  user_privacy: "",
  //用户服务
  user_services: "",
  //内容发布须知
  release_notes: "",
  //客服地址
  kefu_url: ""
}
const getters = {
  occupation_data: state => state.occupation_data,
  interest_label: state => state.interest_label,
  service_label: state => state.service_label,
  picture_label: state => state.picture_label,
  assess_label: state => state.assess_label,
  information_collection: state => state.information_collection,
  third_description: state => state.third_description,
  user_privacy: state => state.user_privacy,
  user_services: state => state.user_services,
  release_notes: state => state.release_notes,
  kefu_url: state => state.kefu_url
}

const mutations = {
  SET_OCCUPATION_DATA: (state, data) => {
    state.occupation_data = data
  },
  SET_INTEREST_LABEL: (state, data) => {
    state.interest_label = data
  },
  SET_SERVICE_LABEL: (state, data) => {
    state.service_label = data
  },
  SET_PICTURE_LABEL: (state, data) => {
    state.picture_label = data
  },
  SET_ASSESS_LABEL: (state, data) => {
    state.assess_label = data
  },
  SET_INFORMATION_COLLECTION: (state, data) => {
    state.information_collection = data
  },
  SET_THIRD_DESCRIPTION: (state, data) => {
    state.third_description = data
  },
  SET_USER_PRIVACY: (state, data) => {
    state.user_privacy = data
  },
  SET_USER_SERVICES: (state, data) => {
    state.user_services = data
  },
  SET_RELEASE_NOTES: (state, data) => {
    state.release_notes = data
  },
  SET_KEFU_URL: (state, data) => {
    state.kefu_url = data
  }


}

const actions = {
  async setting({ commit }) {
    // 调用配置接口
    await getSiteConfig().then(res => {
      commit("SET_OCCUPATION_DATA", res.data.label_data.occupation_data)
      commit("SET_INTEREST_LABEL", res.data.label_data.interest_label)
      commit("SET_SERVICE_LABEL", res.data.label_data.service_label)
      commit("SET_PICTURE_LABEL", res.data.label_data.picture_label)
      commit("SET_ASSESS_LABEL", res.data.label_data.assess_label)
      commit("SET_INFORMATION_COLLECTION", res.data.agreement.information_collection)
      commit("SET_THIRD_DESCRIPTION", res.data.agreement.third_description)
      commit("SET_USER_PRIVACY", res.data.agreement.user_privacy)
      commit("SET_USER_SERVICES", res.data.agreement.user_services)
      commit("SET_RELEASE_NOTES", res.data.agreement.release_notes)
      commit("SET_KEFU_URL", res.data.agreement.kefu_url)
    })
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}