import WebIM from "easemob-websdk";
// import websdk from "./Easemob-chat";
import config from '@/utils/WebIMConfig'
import store from '@/store';
import { enterim } from '@/api/index'
import {
  Toast
} from "vant";
import router from '@/router/index'
import { generateUUID } from '@/utils/fun'
//  const appKey="1120230610161163#seyouplay"


WebIM.config = config;

console.log(WebIM.config);
WebIM.conn = new WebIM.connection({
  appKey: WebIM.config.appkey,
  isHttpDNS: WebIM.config.isHttpDNS,
  isMultiLoginSessions: WebIM.config.isMultiLoginSessions,
  https: WebIM.config.https,
  url: WebIM.config.socketServer,
  apiUrl: WebIM.config.restServer,
  isAutoLogin: WebIM.config.isAutoLogin,
  autoReconnectNumMax: WebIM.config.autoReconnectNumMax,
  autoReconnectInterval: WebIM.config.autoReconnectInterval,
  delivery: WebIM.config.delivery,
  useOwnUploadFun: WebIM.config.useOwnUploadFun
})


// function ack(message) {
//   var bodyId = message.id; // 需要发送已读回执的消息id
//   var msg = new WebIM.message("read", WebIM.conn.getUniqueId());
//   msg.set({
//     id: bodyId,
//     to: message.from,
//   });
//   WebIM.conn.send(msg.body);
// }

// // 初始化IM SDK
// var WebIM = {};
// WebIM = window.WebIM = websdk;

// WebIM.logger.setConfig({
//   useCache: true, // 是否缓存
//   maxCache: 3 * 1024 * 1024, // 最大缓存字节
// });
// // 缓存全部等级日志
// WebIM.logger.setLevel(0);

// WebIM.logger.enableAll();

// WebIM.conn = new WebIM.connection({
//   appKey: appKey,
//   useOwnUploadFun: true,
//   delivery: true,
//   // deviceId:"1234"
// });

// WebIM.conn.listen({
//   onOpened: function () {},          //连接成功回调  
//   onClosed: function () {},         //连接关闭回调
//   onTextMessage: function ( message ) {
//     console.log(message)
//   },    //收到文本消息
// })
WebIM.conn.addEventHandler("connect", {
  onConnected: function (message) {
    // 连接成功回调
    console.log("%c>>>>>>onConnected456789", "color:#9484f7", message);
  },
  onDisconnected: function (message) {
    console.log("%conDisconnected", "color:#9484f7", message);
  }, // 连接关闭回调
  onTextMessage: function (message) {
    console.log("%c>>>>>>>收到文本消息", "color:#9484f7", message);

    store.dispatch("list/defaultData",message)

    store.dispatch("list/regaingetConversationlist",message)

    // if()

    // let messages = this.$store.getters('list/messages')
    // messages.messages.push(message)
    // this.$store.commit('list/SET_messages', {
    //   messages: messages
    // })

    // ack(message)
  }, // 收到文本消息。
  onCombineMessage: function (message) {
    console.log("%c>>>>>>>收到合并消息", "color:#9484f7", message);
  }, // 收到合并消息。
  onEmojiMessage: function (message) {
    console.log("%c>>>>>>>收到表情消息", "color:#9484f7", message);
  }, // 收到表情消息。
  onImageMessage: function (message) {
    console.log("%c>>>>>>>收到图片消息", "color:#9484f7", message);

    store.dispatch("list/defaultData",message)
  }, // 收到图片消息。 
  onCmdMessage: function (message) {
    console.log("%c>>>>>>>收到cmd消息", "color:#9484f7", message);
  }, // 收到命令消息。
  onAudioMessage: function (message) {
    console.log("%c>>>>>>>收到音频消息", "color:#9484f7", message);
  }, // 收到音频消息。
  onLocationMessage: function (message) {
    console.log("%c>>>>>>>收到位置消息", "color:#9484f7", message);
  }, // 收到位置消息。
  onFileMessage: function (message) {
    console.log("%c>>>>>>>收到文件消息", "color:#9484f7", message);
  }, // 收到文件消息。
  onCustomMessage: function (message) {
    console.log("%c>>>>>>>收到自定义消息", "color:#9484f7", message);
  }, // 收到自定义消息。
  onVideoMessage: function (message) {
    var node = document.getElementById('privateVideo');
    var option = {
      url: message.url,
      headers: {
        'Accept': 'audio/mp4'
      },
      onFileDownloadComplete: function (response) {
        var objectURL = WebIM.utils.parseDownloadResponse.call(conn, response);
        node.src = objectURL;
      },
      onFileDownloadError: function () {
        console.log('File down load error.')
      }
    };
    WebIM.utils.download.call(conn, option);
    console.log("%c>>>>>>>收到视频消息", "color:#9484f7", message);
  }, // 收到视频消息。
  onRecallMessage: function (message) {
    console.log("%c>>>>>>>收到消息撤回回执", "color:#9484f7", message);
  }, // 收到消息撤回回执。
  onReceivedMessage: function (message) {
    // console.log("%c>>>>>>>收到消息送达服务器回执", "color:#9484f7", message);
  }, // 收到消息送达服务器回执。
  onDeliveredMessage: function (message) {
    // console.log("%c>>>>>>>收到消息送达客户端回执", "color:#9484f7", message);
  }, // 收到消息送达客户端回执。
  onReadMessage: function (message) {
    console.log("%c>>>>>>>收到消息已读回执", "color:#9484f7", message);
    // store.dispatch("list/defaultData")
    // store.dispatch('list/getConversationlist')
  }, // 收到消息已读回执。
  onMutedMessage: function (message) {
    console.log("%c>>>>>>>用户被禁言", "color:#9484f7", message);
  }, // 如果用户在 A 群组被禁言，在 A 群发消息会触发该回调且消息不传递给该群的其它成员。
  onChannelMessage: function (message) {
    console.log("%c>>>>>>>收到会话已读回执", "color:#9484f7", message);
  }, // 收到会话已读回执，对方发送 `channel ack` 时会触发该回调。

  onPresence: function (message) {
    console.log("%c>>>>>>>“发布-订阅”消息", "color:#9484f7", message);
  }, // 处理“广播”或“发布-订阅”消息，如联系人订阅请求、处理群组、聊天室被踢或解散等消息.
  onRoster: function (message) {
    console.log("%c>>>>>>>处理好友申请", "color:#9484f7", message);
  }, // 处理好友申请。
  onInviteMessage: function (message) {
    console.log("%c>>>>>>>处理群组邀请", "color:#9484f7", message);
  }, // 处理群组邀请。
  onOnline: function () {
    console.log("%c>>>>>>>本机网络连接成功", "color:#9484f7");
  }, // 本机网络连接成功。
  onOffline: function () {
    console.log("%c>>>>>>>本机网络掉线", "color:#9484f7");
  }, // 本机网络掉线。
  onError: function (message) {
    console.log("%c>>>>>>>失败回调1112121", "color:#ef8784", message);
    if (message.type == 28 || message.type == 39 || message.type == 700) {
      enterim().then(ress => {
        store.commit('SET_ACCESS_TOKEN', ress.data.access_token)
        console.log(ress.data.access_token)
        WebIM.conn.open({
          user: store.state.userInfo.uid.toString(),
          accessToken: store.state.access_token,
        }).then(() => {
          console.log("登录成功");
          let path = router.app.$router.currentRoute.path;
          router.push({ path: path })
        }).catch((reason) => {
          store.commit('SET_TOKEN', '');
          store.commit('SET_DID', '');
          store.commit('SET_APP_KEY', '');
          generateUUID();
          //00000000000000
          router.replace('/Login')
        });
      })
    } else if (message.type == 1) {
      router.push({ path: '/Login' })
    }
  }, // 失败回调。
  onReactionChange: function (message) {
    console.log("%c>>>>>>>reaction", "color:#9484f7", message);
  },
  // 收到好友邀请触发此方法。
  onContactInvited: function (message) {
    console.log("%c>>>>>>>收到好友请求", "color:#9484f7", message);
    console.log("from是谁", message.from);
  },
  // 联系人被删除时触发此方法。
  onContactDeleted: function (message) {
    console.log("%c>>>>>>>联系人被删除", "color:#9484f7", message);
  },
  // 新增联系人会触发此方法。
  onContactAdded: function (message) {
    console.log("%c>>>>>>>新增联系人", "color:#9484f7", message);
  },
  // 好友请求被拒绝时触发此方法。
  onContactRefuse: function (message) {
    console.log("%c>>>>>>>好友请求被拒绝", "color:#9484f7", message);
  },
  // 好友请求被同意时触发此方法。
  onContactAgreed: function (message) {
    console.log("%c>>>>>>>好友请求被同意", "color:#9484f7", message);
  },
  onBlocklistUpdate: function (message) {
    //黑名单变动
    // 查询黑名单，将好友拉黑，将好友从黑名单移除都会回调这个函数，list则是黑名单现有的所有好友信息
    console.log("%c>>>>>>>黑名单变动", "color:#9484f7", message);
  },
  onPresenceStatusChange: (message) => {
    // 这里可以处理订阅用户状态更新后的逻辑。
    console.log("%c>>>>>>>订阅用户状态更新", "color:#9484f7", message);
  },
  onGroupChange: (message) => {
    console.log("%c>>>>>>>群组事件", "color:#9484f7", message);

  },
  onGroupEvent: (msg) => {
    console.log("%c>>>>>>>这是群事件监听", "color:#9484f7", msg);

  },
  onChatroomChange(msg) {
    console.log("%c>>>>>>>这是聊天室事件监听", "color:#9484f7", msg);
  },
  // 离线时收到回执，登录后会在这里监听到。
  // onStatisticMessage: (message) => {
  // 	console.log('%c>>>>>>>离线时收到回执', "color:#9484f7", message)
  // }
  onTokenWillExpire: () => {
    console.log("onTokenWillExpire");
  },
  onTokenExpired: () => {
    console.log("onTokenExpired");
  },
});


// // WebIM.conn.removeEventHandler('message')

export default WebIM;
