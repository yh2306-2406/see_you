/*
 * @Author: fleeting-ln 3518413017@qq.com
 * @Date: 2023-11-17 20:26:01
 * @LastEditors: fleeting-ln 3518413017@qq.com
 * @LastEditTime: 2023-12-12 22:35:02
 * @FilePath: \see_you\src\utils\crypto.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/***
 * @Author: XY
 * @Date: 2023-11-15 20:13:00
 * @LastEditors: XY
 * @LastEditTime: 2023-11-20 10:55:17
 * @FilePath: \friendsApp\see_you\src\utils\crypto.js
 **/
import CryptoJS from 'crypto-js'
//前后端需要核对好KEY 、IV 、mode、padding的值，保持一致
const KEY = CryptoJS.enc.Utf8.parse('ayCBoqSuQkzsKbgJ');//十六位十六进制数作为密钥
const IV = CryptoJS.enc.Utf8.parse('FEDCBA0987654321');//十六位十六进制数作为密钥偏移量
/*
 * AES加密 ：字符串 key iv  返回base64
 */
export function encrypt(str, keyStr, ivStr) {
  let key = KEY
  let iv = IV

  if (keyStr && ivStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let srcs = CryptoJS.enc.Utf8.parse(str);
  var encrypt = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,            //这里可以选择AES加密的模式
    padding: CryptoJS.pad.Pkcs7
  });
  return CryptoJS.enc.Base64.stringify(encrypt.ciphertext);
}

/*
 * AES 解密 ：字符串 key iv  返回base64
 */
export function decrypt(str, keyStr, ivStr) {
  let key = KEY
  let iv = IV

  if (keyStr && ivStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let base64 = CryptoJS.enc.Base64.parse(str);
  let src = CryptoJS.enc.Base64.stringify(base64);

  var decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,            //这里可以选择AES解密的模式
    padding: CryptoJS.pad.Pkcs7
  });

  // console.log(decrypt);

  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr?JSON.parse(decryptedStr):{};
}
