/***
 * @Author: XY
 * @Date: 2023-11-15 20:13:00
 * @LastEditors: XY
 * @LastEditTime: 2023-11-15 20:27:04
 * @FilePath: \friendsApp\see_you\src\utils\cryptoJS.js
 **/
import CryptoJS from 'crypto-js'
//前后端需要核对好KEY 、IV 、mode、padding的值，保持一致
const KEY = CryptoJS.enc.Utf8.parse('ayCBoqSuQkzsKbgJ');//十六位十六进制数作为密钥
const IV = CryptoJS.enc.Utf8.parse('FEDCBA0987654321');//十六位十六进制数作为密钥偏移量
/*
 * AES加密 ：字符串 key iv  返回base64
 */
export function myEncrypt(str, keyStr, ivStr) {
  let key = KEY
  let iv = IV

  if (keyStr && ivStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let srcs = CryptoJS.enc.Utf8.parse(str);
  var encrypt = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,            //这里可以选择AES加密的模式
    padding: CryptoJS.pad.Pkcs7
  });
  return CryptoJS.enc.Base64.stringify(encrypt.ciphertext);
}

/*
 * AES 解密 ：字符串 key iv  返回base64
 */
export function myDecrypt(str, keyStr, ivStr) {
  let key = KEY
  let iv = IV

  if (keyStr && ivStr) {
    key = CryptoJS.enc.Utf8.parse(keyStr);
    iv = CryptoJS.enc.Utf8.parse(ivStr);
  }

  let base64 = CryptoJS.enc.Base64.parse(str);
  let src = CryptoJS.enc.Base64.stringify(base64);

  var decrypt = CryptoJS.AES.decrypt(src, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,            //这里可以选择AES解密的模式
    padding: CryptoJS.pad.Pkcs7
  });

  var decryptedStr = decrypt.toString(CryptoJS.enc.Utf8);
  return decryptedStr.toString();
}