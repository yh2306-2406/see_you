/*
 * @Author: fleeting-ln 3518413017@qq.com
 * @Date: 2023-11-14 22:04:15
 * @LastEditors: fleeting-ln 3518413017@qq.com
 * @LastEditTime: 2023-11-17 00:29:14
 * @FilePath: \see_you\src\utils\getLocation.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/***
 * @Author: XY
 * @Date: 2023-11-13 17:52:57
 * @LastEditors: XY
 * @LastEditTime: 2023-11-13 17:53:01
 * @FilePath: \friendsApp\see_you\src\utils\getLocation.js
 **/
function loadSDK() {
  if (window.AMap) return;
  return new Promise((resolve, reject) => {
    const script = document.createElement('script');
    script.src = 'http://webapi.amap.com/maps?v=2.0&key=53ba224dffdf8c6b97c4f78b707bd5b1' ; // ***为申请的高德key
    document.head.appendChild(script);
    script.onload = resolve;
    script.onerror = reject;
  });
}

export default async () => {
  await loadSDK();
  return new Promise((resolve) => {
    AMap.plugin('AMap.Geolocation', () => {
      const geolocation = new AMap.Geolocation({ enableHighAccuracy: false });
      geolocation.getCurrentPosition((status, result) => {
        const res = status === 'complete' ? result.position : { lat: 39.909187, lng: 116.397451 }; // 默认北京 116.397451、39.909187
        console.log('定位结果', res);
        resolve(res);
      });
    });
  });
}

/**

* 高德地图定位

* @type {{}}

*/

export const location = {
  initMap(id) {
    let mapObj = new AMap.Map(id, {})
    let geolocation;
    mapObj.plugin(['AMap.Geolocation'], function () {
      geolocation = new AMap.Geolocation({
        enableHighAccuracy: true, // 是否使用高精度定位，默认:true
        timeout: 10000, // 超过10秒后停止定位，默认：无穷大
        maximumAge: 0, // 定位结果缓存0毫秒，默认：0
        convert: true, // 自动偏移坐标，偏移后的坐标为高德坐标，默认：true
        showButton: true, // 显示定位按钮，默认：true
        buttonPosition: 'LB', // 定位按钮停靠位置，默认：'LB'，左下角
        buttonOffset: new AMap.Pixel(10, 20), // 定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
        showMarker: true, // 定位成功后在定位到的位置显示点标记，默认：true
        showCircle: true, // 定位成功后用圆圈表示定位精度范围，默认：true
        panToLocation: true, // 定位成功后将定位到的位置作为地图中心点，默认：true
        zoomToAccuracy: true // 定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
      })
      mapObj.addControl(geolocation)
      geolocation.getCurrentPosition()
    })
    return geolocation;
  }
}
