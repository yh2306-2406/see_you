/***
 * @Author: XY
 * @Date: 2023-11-09 16:20:14
 * @LastEditors: XY
 * @LastEditTime: 2023-11-15 20:07:28
 * @FilePath: \friendsApp\see_you\vue.config.js
 **/
const path = require("path");

module.exports = {
  // ...其他配置
  configureWebpack: {
    externals: {
      AMap: 'AMap', // 高德地图配置
      AMapUI: 'AMapUI'
    }
  },
  outputDir: "seeyou-app",
  productionSourceMap: false,
  devServer: {
    client: {
      overlay: false,
    },
    host: "0.0.0.0",
    proxy: {
      [process.env.VUE_APP_BASE_API]: {
        target: "https://api.seyou1.vip",
        secure: false,
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      },
    },
  },
  

  lintOnSave: false,
  chainWebpack(config) {
    config.module
      .rule("svg")
      .exclude.add(path.resolve("v2/src/assets/icons"))
      .end();
      config.externals({
        // 'BMap':"BMap",
        AMap: 'AMap', // 高德地图配置
        AMapUI: 'AMapUI'
      })
    config.module
      .rule("icons")
      .test(/\.svg$/)
      .include.add(path.resolve("v2/src/assets/icons"))
      .end()
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({
        symbolId: "icon-[name]"
      });
  },
};
